/**
  * 内積計算
  * @param x 配列
  * @param y 配列
  * @return 内積
  *
  * ２つの配列x, yの長さが異なる場合は例外を投げる
  */
function dot(x, y) {
    if (x.length !== y.length) {
        err = new Error();
        console.log('x.length = ' + x.length);
        console.log('y.length = ' + y.length);
        err.message = "２つのベクトルの次元が一致していません"
        throw err;
    }
    ret = 0;
    for (var i=0; i < x.length; ++i) {
        ret += x[i] * y[i];
    }
    return ret;
}

/**
  * 正規分布の計算
  * @param mu 平均値
  * @param sigma 分散
  * @return 正規分布のサンプル
  */
function norm(mu, sigma) {
    var a = 1 - Math.random();
    var b = 1 - Math.random();
    var c = Math.sqrt(-2 * Math.log(a));
    if(0.5 - Math.random() > 0) {
        return c * Math.sin(Math.PI * 2 * b) * sigma + mu;
    }else{
        return c * Math.cos(Math.PI * 2 * b) * sigma + mu;
    }
}

function modify_labels(Data) {
    for (var i=0; i < Data.length; ++i) {
        if (Data[i].y === -1) {
            Data[i].y = 0;
        }
    }
    return Data;
}

function printStackTrace(e) {
  if (e.stack) {
    // 出力方法は、使いやすいように修正する。
    console.log(e.stack);
    alert(e.stack);
  } else {
    // stackがない場合には、そのままエラー情報を出す。
    console.log(e.message, e);
  }
}
