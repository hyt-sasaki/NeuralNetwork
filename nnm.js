/**
  * ニューラルネットワークモデル
  */
var NeuralNetworkModel = (function () {
    /**
      * コンストラクタ
      * @param d_i 入力ベクトルの次元数
      * @param d_h 隠れ層の数
      * @param d_o 出力ベクトルの次元数
      */
    var NeuralNetworkModel = function (d_i, d_h, d_o) {
        this.d_i = d_i;
        this.d_h = d_h;
        this.d_o = d_o;
        this.initialize();
    }

    var pt = NeuralNetworkModel.prototype;

    /**
      * 初期化関数
      * this.hidden_neuronsおよびthis.out_neuronsを初期化する
      */
    pt.initialize = function () {
        this.hidden_neurons = [];
        this.out_neurons = [];

        /* 重み初期化パラメータ */
        var mu = 0.0;       // 平均値
        var sigma = 1.0;    // 分散

        /* 隠れ層ニューロンの初期化 */
        for (var h=0; h < this.d_h; ++h) {
            var weights = [];
            for (var i=0; i <= this.d_i; ++i) {
                weights.push(norm(mu, sigma));
            }
            var hidden_neuron = new NeuronModel(sigmoid, weights);
            this.hidden_neurons.push(hidden_neuron);
        }

        /* 出力層ニューロンの初期化 */
        for (var o=0; o < this.d_o; ++o) {
            var weights = [];
            for (var h=0; h <= this.d_h; ++h) {
                weights.push(norm(mu, sigma));
            }
            var out_neuron = new NeuronModel(sigmoid, weights);
            this.out_neurons.push(out_neuron);
        }
    }

    /**
      * 隠れ層の出力値計算関数
      * @param x ニューラルネットワークへの入力値配列
      * @return 隠れ層の出力値配列
      */
    pt.out_h = function (x) {
        var hidden_inputs = [1].concat(x);    // バイアス項の計算用に1を先頭に挿入
        var hidden_outs = [];
        for (var i=0; i < this.hidden_neurons.length; ++i) {
            hidden_outs.push(this.hidden_neurons[i].out(hidden_inputs));
        }
        return hidden_outs;
    }

    /**
      * 出力層の出力値計算関数
      * @param x ニューラルネットワークへの入力値配列
      * @return 出力層の出力値配列
      */
    pt.out_o = function (x) {
        var hidden_outs = this.out_h(x);
        var out_inputs = [1].concat(hidden_outs);    //バイアス項の計算用に1を先頭に挿入
        var out_outs = [];
        for (var o=0; o < this.out_neurons.length; ++o) {
            out_neuron = this.out_neurons[o];
            out_outs.push(out_neuron.out(out_inputs));
        }
        return out_outs;
    }

    pt.judge = function (x) {
        var out_outs = this.out_o(x);
        if (this.d_o === 1) {
            if (out_outs[0] >= 0.5) {
                return 1;
            } else {
                return 0;
            }
        } else {
            var max = -1;
            var ret = -1;
            for (var k=0; k < out_outs.length; ++k) {
                if (out_outs[k] > max) {
                    max = out_outs[k];
                    ret = k;
                }
            }
            return ret;
        }
    }

    return NeuralNetworkModel;
})();
