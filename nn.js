var NeuralNetwork = (function () {
    /* コンストラクタ */
    var NeuralNetwork = function (nnm, e, a, i) {
        this.nnm = nnm; //ニューラルネットワークモデル
        this.eta = e; //学習率
        this.alpha = a;   //スケジューリングファクタ
        this.itr = i;   //学習回数
        this.cost_type = 'cross_entropy';

        this.nnm.initialize();
    }

    var pt = NeuralNetwork.prototype;

    /* 学習 */
    pt.train = function (Data) {
        var choice = [];
        for (var i=0; i < Data.length; ++i) {
            choice[i] = i;
        }

        for (var i=0; i < this.itr; ++i) {
            var c = choice.slice();
            while(c.length > 0) {
                var rnd = Math.floor(Math.random() * c.length);
                var d = Data[c[rnd]];

                c.splice(rnd, 1);

                this.update(d);
            }
            this.eta *= this.alpha;
            console.log('i = ' + i + ': ' + this.cost(Data));
        }
    }

    /* 更新 */
    pt.update = function (d) {
        var hidden_outs = this.nnm.out_h(d.x);
        var out_outs = this.nnm.out_o(d.x);

        var hidden_inputs = [1].concat(d.x);
        var out_inputs = [1].concat(hidden_outs)

        var hidden_errors = [];
        var out_errors = [];

        /* バックプロパゲーション */
        /* 出力層のエラー計算 */
        for (var o=0; o < out_outs.length; ++o) {
            var y;
            if (this.nnm.d_o === 1) {
                y = d.y
            } else {
                y = d.y[k]
            }
            out_error = output_error(y, out_outs[o], 'cross_entropy');
            out_errors.push(out_error);
        }
        /* 隠れ層のエラー計算 */
        for (var h=0; h < hidden_outs.length; ++h) {
            var out_errors_sum = 0;
            for (var o=0; o < this.nnm.out_neurons.length; ++o) {
                out_errors_sum += this.nnm.out_neurons[o].w[h] * out_errors[o];
            }
            hidden_error = hidden_outs[h] * (1 - hidden_outs[h]) * out_errors_sum;
            hidden_errors.push(hidden_error);
        }

        /* パラメータ更新 */
        /* 出力層のパラメータ更新 */
        for (var o=0; o < this.nnm.out_neurons.length; ++o) {
            out_error = out_errors[o];
            for (var h=0; h < this.nnm.out_neurons[o].w.length; ++h) {
                out_input = out_inputs[h];
                this.nnm.out_neurons[o].w[h] -= this.eta * out_error * out_input;
            }
        }

        for (var h=0; h < this.nnm.hidden_neurons.length; ++h) {
            hidden_error = hidden_errors[h];
            for (var i=0; i < this.nnm.hidden_neurons[h].w.length; ++i) {
                hidden_input = hidden_inputs[i];
                this.nnm.hidden_neurons[h].w[i] -= this.eta * hidden_error * hidden_input;
            }
        }
    }

    /* エラーチェック */
    pt.check = function (Data) {
        var error = Data.length;
        for (var i=0; i < Data.length; ++i) {
            y = Data[i].y;
            if (this.nnm.judge(Data[i].x) === y) {
                error--;
            }
        }
        return error;
    }

    pt.cost = function (Data) {
        cost = 0;
        for (var i=0; i < Data.length; ++i) {
            y = Data[i].y;
            o = this.nnm.out_o(Data[i].x)[0];
            if (this.cost_type === 'cross_entropy') {
                cost += cross_entropy(y, o);
            } else if (this.cost_type === 'mean_squared') {
                cost += mean_squared(y, o);
            } else {
                cost += 0;
            }
        }
        cost /= Data.length;
        return cost;
    }

    return NeuralNetwork;
})();

function mean_squared(y, o) {
    return Math.pow((y - o), 2) / 2.0;
}

function cross_entropy(y, o) {
    return -(y * Math.log(clip(o)) + (1 - y) * Math.log(clip(1 - o)));
}

function clip(x) {
    if (x <= 0) {
        return 0.0000000001;
    } else {
        return x;
    }
}

function output_error(y, o, cost_type) {
    if (cost_type === 'cross_entropy') {
        return o - y;
    } else if (cost_type === 'mean_squared') {
        return o * (1 - o) * (o - y);
    } else {
        return -1;
    }
}
