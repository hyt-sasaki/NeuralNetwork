/**
  * ニューロンモデル
  */
var NeuronModel = (function() {
    /**
      * コンストラクタ
      * @param a 活性化関数
      * @param w 重みの初期値配列
      */
    var NeuronModel = function(a, w) {
        this.activate = a;
        this.w = w;
    }

    var pt = NeuronModel.prototype;

    /**
      * ニューロンの出力計算
      * @param x 入力値配列
      * @return ニューロンの出力値
      */
    pt.out = function(x) {
        return this.activate(dot(this.w, x));
    }

    /**
      * クラス判定関数
      * @param x 入力値配列
      * @return クラス(0 or 1)
      */
    pt.judge = function(x) {
        if (this.activate === sigmoid) {
            console.log('sigmoid');
            if (this.out(x) >= 0.5) {
                return 1;
            } else {
                return 0;
            }
        } else if (this.activate === step) {
            console.log('step');
            return this.out(x);
        } else {
            console.log('else');
        }
    }

    return NeuronModel;
})();

/**
  * ステップ関数
  * @param x 入力値
  * @return ステップ関数の出力(0 or 1)
  */
function step(x) {
    if (x >= 0) {
        return 1;
    } else {
        return 0;
    }
}

/**
  * シグモイド関数
  * @param x 入力値
  * @return シグモイド関数の出力(0 ~ 1の実数値)
  */
function sigmoid(x) {
    return 1.0 / (1.0 + Math.exp(-x));
}
