/* 点の描画
* d: データ
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawPoint(d, canvas, r) {
    var tLabelColor = 'rgb(255, 0, 0)';
    var fLabelColor = 'rgb(0, 0, 255)';
    ctx = canvas.getContext('2d');
    var c = dataToCanvas(d.x, canvas, r);

    ctx.beginPath();
    if (d.y === 1) {
        ctx.strokeStyle = tLabelColor;
    } else {
        ctx.strokeStyle = fLabelColor;
    }
    ctx.arc(c[0], c[1], canvas.width / 125, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
}

/* 点群の描画
* Data: データ集合
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawPoints(Data, canvas, r) {
    for (var i=0; i < Data.length; ++i) {
        var d = Data[i];
        drawPoint(d, canvas, r);
    }
}

/* 関数の描画
* m: 関数のモデル(線形のみ)
* canvas: canvasオブジェクト
* r: 座標系の定義幅
* color: 描画する曲線の色
*/
function drawFunction(m, canvas, r, color) {
    ctx = canvas.getContext('2d');
    ctx.strokeStyle = color;

    N = 100;

    ctx.beginPath();
    x0 = [-r[0] / 2, m.h(-r[0] / 2)];
    c0 = dataToCanvas(x0, canvas, r);
    ctx.moveTo(c0[0], c0[1]);
    for (var i=1; i <= N; ++i) {
        x = [-r[0] / 2 + r[0] / N * i, m.h(-r[0] / 2 + r[0] / N * i)];
        c = dataToCanvas(x, canvas, r);
        ctx.lineTo(c[0], c[1]);
    }
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
}

/* 識別領域の描画
*
*/
function drawDiscriminant(canvas, r, m) {
    var tLabelColor = 'rgb(255, 0, 0)';
    var fLabelColor = 'rgb(0, 0, 255)';
    var xNum = 100;
    var yNum = 100;
    var rectWidth = canvas.width / xNum;
    var rectHeight = canvas.height / yNum;
    for (var i=0; i < xNum; ++i) {
        for (var j=0; j < yNum; ++j) {
            var rectLeft = rectWidth * i;
            var rectTop = rectHeight * j;
            var c = getRectCenter(rectWidth, rectHeight, rectLeft, rectTop);
            var x = canvasToData(c, canvas, r);
            var color;
            if (m.judge(x) === 1) {
                color = tLabelColor;
            } else {
                color = fLabelColor;
            }
            drawCross(rectWidth, rectHeight, rectLeft, rectTop, canvas, color);
        }
    }
}

function drawCross(w, h, l, t, canvas, color) {
    /* バツ印の左上座標 */
    var x1 = l;
    var y1 = t;
    /* バツ印の右下座標 */
    var x2 = l + w;
    var y2 = t + h;
    /* バツ印の右上座標 */
    var x3 = l + w;
    var y3 = t;
    /* バツ印の左下座標 */
    var x4 = l;
    var y4 = t + h;

    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.moveTo(x3, y3);
    ctx.lineTo(x4, y4);
    ctx.strokeStyle = color;
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
}

function getRectCenter(w, h, l, t) {
    return [l + w / 2, t + h / 2];
}

/* 座標軸の描画
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawAxes(canvas, r) {
    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    /* x軸の描画 */
    ctx.moveTo(0, canvas.height / 2);
    ctx.lineTo(canvas.width, canvas.height / 2);
    ctx.moveTo(canvas.width, canvas.height / 2);
    ctx.lineTo(canvas.width * 108 / 110, canvas.height * 53 / 110);
    ctx.lineTo(canvas.width * 108 / 110, canvas.height * 57 / 110);
    ctx.closePath();
    ctx.moveTo(canvas.width * 105 / 110, canvas.height * 54 / 110);
    ctx.lineTo(canvas.width * 105 / 110, canvas.height * 56 / 110);
    ctx.moveTo(canvas.width * 5 / 110, canvas.height * 54 / 110);
    ctx.lineTo(canvas.width * 5 / 110, canvas.height * 56 / 110);

    /* y軸の描画 */
    ctx.moveTo(canvas.width / 2, canvas.height);
    ctx.lineTo(canvas.width / 2, 0);
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width * 53 / 110, canvas.height * 2 / 110);
    ctx.lineTo(canvas.width * 57 / 110, canvas.height * 2 / 110);
    ctx.closePath();
    ctx.moveTo(canvas.width * 54 / 110, canvas.height * 5 / 110);
    ctx.lineTo(canvas.width * 56 / 110, canvas.height * 5 / 110);
    ctx.moveTo(canvas.width * 54 / 110, canvas.height * 105 / 110);
    ctx.lineTo(canvas.width * 56 / 110, canvas.height * 105 / 110);

    ctx.stroke();
    ctx.fill();

    /* 文字の描画 */
    var normalSize = Math.min(canvas.width, canvas.height) / 20;
    var subscriptSize = Math.floor(normalSize * 0.5);
    var fontname = String(normalSize) + "px 'Times New Roman'";
    var _fontname = String(subscriptSize) + "px 'Times New Roman'";
    var fontnameItalic = "Italic " + fontname
    var _fontnameItalic = "Italic " + _fontname

    ctx.font = fontnameItalic;
    ctx.textAlign = 'left';
    ctx.textBaseline = 'middle';
    ctx.fillText('x', canvas.width * 58 / 110, canvas.height * 2 / 110);
    ctx.font = _fontname;
    ctx.textBaseline = 'bottom';
    var metrics = ctx.measureText('x');
    ctx.fillText('2', canvas.width * 58 / 110 + metrics.width * 1.5, (canvas.height * 2 / 110) + normalSize / 2);
    ctx.textAlign = 'right';
    ctx.textBaseline = 'middle';
    ctx.font = fontname;
    ctx.fillText(String(r[0] / 2), canvas.width * 52 / 110, canvas.height * 5 / 110);
    ctx.fillText('-' + String(r[0] / 2), canvas.width * 52 / 110, canvas.height * 105 / 110);

    ctx.font = fontnameItalic;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'bottom';
    ctx.fillText('x', canvas.width * 108 / 110, canvas.height * 52 / 110);
    ctx.font = _fontname;
    ctx.fillText('1', canvas.width * 108 / 110 + metrics.width * 1.1, (canvas.height * 52 / 110));

    ctx.font = fontname;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'top';
    ctx.fillText(String(r[1] / 2), canvas.width * 105 / 110, canvas.height * 58 / 110);
    ctx.fillText('-' + String(r[1] / 2), canvas.width * 5 / 110, canvas.height * 58 / 110);
    ctx.textAlign = 'right';
    ctx.textBaseline = 'top';
    ctx.fillText('0', canvas.width * 54 / 110, canvas.height * 56 / 110);
}

/* 座標系の変換 */
function dataToCanvas(x, canvas, r) {
    w = canvas.width * (1 / 2 + x[0] / r[0]);
    h = canvas.height * (1 / 2 - x[1] / r[1]);
    return [w, h];
}

function canvasToData(c, canvas, r) {
    x = r[0] * (c[0] / canvas.width - 1 / 2);
    y = r[1] * (-c[1] / canvas.height + 1 / 2);
    return [x, y];
}

function drawNetworkTemplate(canvas) {
    var nodeRadius = Math.min(canvas.width, canvas.height) * 0.05;
    var W = canvas.width * 0.4;
    var wOffset = (canvas.width - W * 2) / 2;
    var H = canvas.height * 0.26;
    var hOffset = (canvas.height - H * 3) / 2;

    var iLayerPos = [[wOffset, canvas.height / 2 - H], [wOffset, canvas.height / 2], [wOffset, canvas.height / 2 + H]];
    var hLayerPos = [[canvas.width / 2, canvas.height / 2 - H * 3 / 2], [canvas.width / 2, canvas.height / 2 - H / 2], [canvas.width / 2, canvas.height / 2 + H * 3 / 2]];
    var oLayerPos = [[canvas.width - wOffset, canvas.height / 2]];

    var ctx = canvas.getContext('2d');
    ctx.beginPath();
    for (var i=0; i < iLayerPos.length; ++i) {
        for (var j=1; j < hLayerPos.length; ++j) {
            ctx.moveTo(iLayerPos[i][0], iLayerPos[i][1]);
            ctx.lineTo(hLayerPos[j][0], hLayerPos[j][1]);
            ctx.lineTo(oLayerPos[0][0], oLayerPos[0][1]);
        }
    }
    ctx.moveTo(hLayerPos[0][0], hLayerPos[0][1]);
    ctx.lineTo(oLayerPos[0][0], oLayerPos[0][1]);
    ctx.stroke();

    ctx.fillStyle = 'rgb(255, 255, 255)';
    for (var i=0; i < iLayerPos.length; ++i) {
        ctx.beginPath();
        ctx.arc(iLayerPos[i][0], iLayerPos[i][1], nodeRadius, 0, Math.PI * 2, true);
        ctx.fill();
        ctx.stroke();
    }
    for (var j=0; j < hLayerPos.length; ++j) {
        ctx.beginPath();
        ctx.arc(hLayerPos[j][0], hLayerPos[j][1], nodeRadius, 0, Math.PI * 2, true);
        ctx.fill();
        ctx.stroke();
    }
    ctx.beginPath();
    ctx.arc(oLayerPos[0][0], oLayerPos[0][1], nodeRadius, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.stroke();
    ctx.fillStyle = 'rgb(0, 0, 0)';

    var pointsInterval = H / 3;
    var pointsRadius = H / 30;
    var pointsPos = [[canvas.width / 2, canvas.height / 2 + H / 2 - pointsInterval],[canvas.width / 2, canvas.height / 2 + H / 2],[canvas.width / 2, canvas.height / 2 + H / 2 + pointsInterval]];

    for (var i=0; i < pointsPos.length; ++i) {
        ctx.beginPath();
        ctx.arc(pointsPos[i][0], pointsPos[i][1], pointsRadius, 0, Math.PI * 2, true);
        ctx.fill();
    }

    var fontSize = Math.floor(nodeRadius * 2 * 0.7);
    var fontname = fontSize + "px 'Times New Roman'";
    var fontnameItalic = 'Italic ' + fontname;
    var _fontSize = fontSize * 0.5;
    var _fontname=  _fontSize + "px 'Times New Roman'";
    var _fontnameItalic = 'Italic ' + _fontname;
    ctx.fillStyle = 'rgb(0, 0, 0)';
    var iLayerLabel = [['1', ''], ['x', '1'], ['x', '2']];
    for (var i=0; i < iLayerPos.length; ++i) {
        if (i === 0) {
            ctx.font = fontname;
        } else {
            ctx.font = fontnameItalic;
        }
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        var textOffset = (i !== 0) ? nodeRadius * 0.2: 0;
        ctx.fillText(iLayerLabel[i][0], iLayerPos[i][0] - textOffset, iLayerPos[i][1]);
        var metrics = ctx.measureText(iLayerLabel[i][0]);
        ctx.font = _fontname;
        ctx.textAlign = 'start';
        ctx.textBaseline = 'bottom';
        ctx.fillText(iLayerLabel[i][1], iLayerPos[i][0] - textOffset + metrics.width / 2, iLayerPos[i][1] + fontSize / 2);
    }
    var hLayerLabel = [['1', ''], ['h', '1'], ['h', 'M']];
    for (var j=0; j < hLayerPos.length; ++j) {
        if (j === 0) {
            ctx.font = fontname;
        } else {
            ctx.font = fontnameItalic;
        }
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        var textOffset = (j !== 0) ? nodeRadius * 0.2: 0;
        ctx.fillText(hLayerLabel[j][0], hLayerPos[j][0] - textOffset, hLayerPos[j][1]);
        var metrics = ctx.measureText(hLayerLabel[j][0]);
        ctx.font = _fontname;
        ctx.textAlign = 'start';
        ctx.textBaseline = 'bottom';
        ctx.fillText(hLayerLabel[j][1], hLayerPos[j][0] - textOffset + metrics.width / 2, hLayerPos[j][1] + fontSize / 2);
    }

    ctx.font = fontnameItalic;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillText('o', oLayerPos[0][0], oLayerPos[0][1]);

    var weightLabels = [['w', 'ij'], ['w', 'j']];
    var weightLabelPos = [[wOffset + W / 2, hOffset], [wOffset + W * 3 / 2, hOffset]];
    ctx.fillStyle = 'rgb(0, 0, 0)';
    for (var i=0; i < 2; ++i) {
        ctx.font = fontnameItalic;
        ctx.textAlign = 'center';
        ctx.textBaseline = 'bottom';
        var metrics = ctx.measureText(weightLabels[i][0]);
        var textOffset = metrics.width * 0.2;
        ctx.fillText(weightLabels[i][0], weightLabelPos[i][0] - textOffset, weightLabelPos[i][1]);
        ctx.font = _fontnameItalic;
        ctx.textAlign = 'start';
        ctx.textBaseline = 'bottom';
        ctx.fillText(weightLabels[i][1], weightLabelPos[i][0] - textOffset + metrics.width / 2, weightLabelPos[i][1]);
    }
}
