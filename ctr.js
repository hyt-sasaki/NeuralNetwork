$(function () {
    /* 訓練データの設定 */
    var RANGE = [20, 20];   //座標系の定義域幅
    var COLOR_T = 'rgb(255, 0, 0)';
    var COLOR_F = 'rgb(0, 0, 255)';
    (function () {
        var axesCanvas = $('#axesCanvas')[0];
        drawAxes(axesCanvas, RANGE);
        var nnCanvas = $('#nnCanvas')[0];
        drawNetworkTemplate(nnCanvas);
    })();

    var dataRef = {Data:null};
    var fileRef = {file:null};
    var dataCanvas = $('#dataCanvas')[0];
    var modelCanvas = $('#modelCanvas')[0];

    /* イベントハンドラの設定 */
    $('#dataCanvas').on('click', {dataRef:dataRef, canvas:dataCanvas, range:RANGE, color_t:COLOR_T, color_f:COLOR_F}, onCanvasClicked);
    $('#learn').on('click', {dataRef:dataRef, canvas:modelCanvas, range:RANGE, color_t:COLOR_T, color_f:COLOR_F}, onTrain);
    $('#resetButton').on('click', {dataRef:dataRef, dataCanvas:dataCanvas, modelCanvas:modelCanvas}, onReset);
    $('#genFromFile').on('click', {dataRef:dataRef, fileRef:fileRef, canvas:dataCanvas, range:RANGE, color_t:COLOR_T, color_f:COLOR_F}, onGenerateFromFile);
    $('#download').on('click', {dataRef:dataRef}, onDownload);
    $('#openFile').on('change', {fileRef:fileRef}, onFileOpen);
    $(window).on('keydown', onShiftkeyDown);
    $(window).on('keydown', onEnterkeyDown);
});

function onShiftkeyDown(event) {
    if (event.shiftKey) {
        if($('input[name="clickLabel"]:eq(0)').prop('checked')) {
            $('input[name="clickLabel"]:eq(1)').prop('checked', true);
        } else {
            $('input[name="clickLabel"]:eq(0)').prop('checked', true);
        }
    }
}

function onEnterkeyDown(event) {
    if (event.which === 13) {
        $('#learn').trigger('click');
    }
}

function onCanvasClicked(event) {
    var canvas = event.data.canvas;
    var range = event.data.range;
    var canvasRect = canvas.getBoundingClientRect();
    var color_t = event.data.color_t;
    var color_f = event.data.color_f;
    var cx = event.clientX - canvasRect.left;
    var cy = event.clientY - canvasRect.top;
    var c = [cx, cy];
    var x = canvasToData(c, canvas, range);
    var y = ($('input[name="clickLabel"]:checked').val() === "positive") ? 1: -1;
    var d = {x:x, y:y};

    if (event.data.dataRef.Data === null) {
        event.data.dataRef.Data = [];
    }
    event.data.dataRef.Data.push(d);
    drawPoint(d, canvas, range, color_t, color_f);
}

function onTrain(event) {
    alert('学習開始');
    var range = event.data.range;
    var canvas = event.data.canvas;
    var color_t = event.data.color_t;
    var color_f = event.data.color_f;

    var itr = Number($('#itr').val());
    var d_i = 2;
    var d_o = 1;
    var n_h = Number($('#n_h').val());
    var eta = Number($('#eta').val());
    var alpha = Number($('#alpha').val());
    var m_l = new NeuralNetworkModel(d_i, n_h, d_o);
    var l = new NeuralNetwork(m_l, eta, alpha, itr);
    Data = event.data.dataRef.Data;
    Data = modify_labels(Data);
    l.train(Data);

    out = $('#output span');
    out.children().remove();
    out.append($('<sapn>').html('<br />$N_{error} = ' + (l.check(Data) + '$')));

    MathJax.Hub.Queue(["Typeset", MathJax.Hub, out.id]);

    modelCanvas.getContext('2d').clearRect(0, 0, canvas.width, modelCanvas.height);
    drawDiscriminant(canvas, range, m_l, color_t, color_f);
}

function onReset(event) {
    event.data.dataRef.Data = [];
    var dataCanvas = event.data.dataCanvas;
    var modelCanvas = event.data.modelCanvas;
    dataCanvas.getContext('2d').clearRect(0, 0, dataCanvas.width, dataCanvas.height);
    modelCanvas.getContext('2d').clearRect(0, 0, modelCanvas.width, modelCanvas.height);
    out = $('#output span');
    out.children().remove();
}
